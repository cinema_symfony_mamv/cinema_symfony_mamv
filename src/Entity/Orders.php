<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="orders")
     */
    private $users_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MovieOrder", mappedBy="order_id")
     */
    private $movieOrders;

    public function __construct()
    {
        $this->movieOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUsersId(): ?Users
    {
        return $this->users_id;
    }

    public function setUsersId(?Users $users_id): self
    {
        $this->users_id = $users_id;

        return $this;
    }

    /**
     * @return Collection|MovieOrder[]
     */
    public function getMovieOrders(): Collection
    {
        return $this->movieOrders;
    }

    public function addMovieOrder(MovieOrder $movieOrder): self
    {
        if (!$this->movieOrders->contains($movieOrder)) {
            $this->movieOrders[] = $movieOrder;
            $movieOrder->setOrderId($this);
        }

        return $this;
    }

    public function removeMovieOrder(MovieOrder $movieOrder): self
    {
        if ($this->movieOrders->contains($movieOrder)) {
            $this->movieOrders->removeElement($movieOrder);
            // set the owning side to null (unless already changed)
            if ($movieOrder->getOrderId() === $this) {
                $movieOrder->setOrderId(null);
            }
        }

        return $this;
    }
}
