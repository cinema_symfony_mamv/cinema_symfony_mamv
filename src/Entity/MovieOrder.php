<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieOrderRepository")
 */
class MovieOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Movies", inversedBy="movieOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $movie_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Orders", inversedBy="movieOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $order_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMovieId(): ?Movies
    {
        return $this->movie_id;
    }

    public function setMovieId(?Movies $movie_id): self
    {
        $this->movie_id = $movie_id;

        return $this;
    }

    public function getOrderId(): ?Orders
    {
        return $this->order_id;
    }

    public function setOrderId(?Orders $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }
}
