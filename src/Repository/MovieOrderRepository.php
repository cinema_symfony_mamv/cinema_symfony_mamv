<?php

namespace App\Repository;

use App\Entity\MovieOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MovieOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovieOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovieOrder[]    findAll()
 * @method MovieOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovieOrder::class);
    }

    // /**
    //  * @return MovieOrder[] Returns an array of MovieOrder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MovieOrder
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
