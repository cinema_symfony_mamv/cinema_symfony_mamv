<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Movies;
use App\Entity\Users;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $movie = new Movies();
        $movie->setTitle("DemoFilm");
        $movie->setResume("Resumé de test n°1");
        $movie->setYear("2000");
        $movie->setReleased("JJ-MM-AAAA");
        $movie->setRuntime("HH:mm");
        $movie->setGenre("Action, Aventure, Documentaire, Drame");
        $movie->setDirector("Michel Drucker");
        $movie->setLanguage("Français");
        $movie->setCountry("France");
        $movie->setAwards("N/A");
        $movie->setPoster("https://media.giphy.com/media/l3q2K5jinAlChoCLS/giphy.gif");
        $movie->setType("Film");
        $movie->setDvd("01-Juin-2001");
        $movie->setProduction("France Télévision");
        $movie->setAvalaible(true);
        $manager->persist($movie);

        $user = new Users();
        $user->setName("Toto");
        $user->setEmail("mail@exemple.net");
        $user->setPassword("mdp@123");
        $manager->persist($user);

        $manager->flush();
    }
}
